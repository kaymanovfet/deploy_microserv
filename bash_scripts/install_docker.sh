#!/bin/bash

sudo apt update

# install docker
echo --- Step 1. Installing docker
sudo apt install -y docker.io
MYUSER=`whoami`

sudo usermod -aG docker $MYUSER
#newgrp docker
