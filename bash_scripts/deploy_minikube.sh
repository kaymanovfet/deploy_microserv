#!/bin/bash

echo --- Step 2. Installing minikube
curl -Lo minikube https://storage.googleapis.com/minikube/releases/v1.22.0/minikube-linux-amd64
sudo chmod +x minikube
sudo mv minikube /usr/local/bin/


## install kubectl
echo --- Step 3. Installing kubectl
curl -Lo kubectl https://storage.googleapis.com/kubernetes-release/release/v1.21.0/bin/linux/amd64/kubectl
chmod +x kubectl && sudo cp kubectl /usr/local/bin/ && rm kubectl


## start minikube
echo --- Step 4. Starting minikube
minikube start --driver=docker --memory 4098 --addons dashboard metrics-server default-storageclass storage-provisioner

## enable dashboard
echo --- Step 5. Enable minikube dashboard

#minikube addons enable ingress
#minikube addons enable metrics-server

minikube addons enable dashboard

##minikube dashboard --url &

##pkill -f "kubectl"

## run proxy to forward traffic to dashboard
echo --- Step 6. Run proxy for dashboard
kubectl proxy --address='0.0.0.0' --port=21021 --disable-filter=true & echo --- End
