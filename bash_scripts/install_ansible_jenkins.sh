#!/bin/bash

# Обновляем пакетный менеджер apt
echo --Step 1 install apt-get--
sudo apt-get update
sudo apt-get -y upgrade

echo --Step 2 python2 to python 3--
sudo ln -sfn /usr/bin/python3 /usr/bin/python

# Устанавливаем python pip
echo --Step 3 install python-pip--
sudo apt-get install -y python-pip

# Устанавливаем ansible
echo --Step 4 install ansible--
sudo apt-get install ansible

# Устанавливаем OpenJDK
echo --Step 5 install openjdk--
sudo apt-get install -y default-jdk

# Устанавливаем wget and ufw
echo --Step 6 install wget and ufw
sudo apt-get install -y wget
sudo apt-get install -y ufw

# Устанавливаем и запускаем Jenkins
echo --Step 7 install jenkins--
sudo wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt-get update
sudo apt-get install -y jenkins
sudo systemctl start jenkins
sudo service jenkins status
sudo ufw allow OpenSSH
sudo ufw allow 8080
sudo ufw status
#sudo ufw enable
